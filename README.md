# Approximate Bayesian Computation

## Bayesian Inference beyond MCMC

This repository contains an implementation of Approximate Bayesian Computation to infer $\gamma$ rate from an exponential distribution 

Summary statistics comparisons plot: \
<img
  src="res/comparisons.png"
  alt="Alt text"
  title=""
  style="display: inline-block; margin: 0 auto; max-width: 250px">
